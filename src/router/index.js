import { createRouter, createWebHashHistory } from 'vue-router'
import Accueil from '../views/Accueil.vue'
import Boutique from '../views/Boutique.vue'
import Monde from '../views/Monde.vue'
import Pnj from '../views/Pnj.vue'


const routes = [
  {
    path: '/',
    name: 'Accueil',
    component: Accueil
  },
  {
    path: '/boutique',
    name: 'Boutique',
    component: Boutique
  },
  {
    path: '/monde',
    name: 'Monde',
    component: Monde
  },
  {
    path: '/pnj',
    name: 'Pnj',
    component: Pnj
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
